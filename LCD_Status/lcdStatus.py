#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import * 
from time import sleep, strftime
from datetime import datetime

lcd = Adafruit_CharLCD()

cmd = "ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1"
load = 'awk \'{printf $1 " " $2 " " $3}\' /proc/loadavg'
uptime = 'uptime | awk \'{printf "Uptime " $3 " " $4}\''
cpuTemp = '/opt/vc/bin/vcgencmd measure_temp | awk \'BEGIN { FS = "=" } ; { print $2 }\''
cpuVolts = '/opt/vc/bin/vcgencmd measure_volts | awk \'BEGIN { FS = "=" } ; { print $2 }\''

lcd.begin(16,1)

def run_cmd(cmd):
    p = Popen(cmd, shell=True, stdout=PIPE)
    output = p.communicate()[0]
    return output

while 1:
    lcd.clear()
    ipaddr = run_cmd(cmd)
    loadResult = run_cmd(load)
    lcd.message('Network IP\n')
    lcd.message(ipaddr)
    sleep(5)
    
    lcd.clear()
    lcd.message('Load Averages\n')        
    lcd.message(loadResult)
    sleep(5)
    
    lcd.clear()
    cpuTempGet = run_cmd(cpuTemp)
    cpuVoltsGet = run_cmd(cpuVolts)
    lcd.message('CPU Temp: ' + cpuTempGet + '\n')
    lcd.message('CPU Volts: ' + cpuVoltsGet)
    sleep(5)
    
    lcd.clear()
    uptimeRes = run_cmd(uptime)
    lcd.message(uptimeRes + '\n')
    sleep(5)