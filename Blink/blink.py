import RPi.GPIO as GPIO
from time import sleep
GPIO.cleanup()
RED_PIN = 23
GREEN_PIN = 18

print "Setting up GPIO"
GPIO.setmode(GPIO.BCM)
GPIO.setup(RED_PIN, GPIO.OUT)
GPIO.setup(GREEN_PIN, GPIO.OUT)

class piClas():
        red = True
        green = False
        
        def enable_red(self,should_enable):
                if should_enable:
                        GPIO.output(RED_PIN, True)
                        self.red = True
                        self.green = False
                        #print "enable red "
                else:
                        GPIO.output(RED_PIN, False)
                        self.green = True
                        self.red = False
                        #print "disable red "

        def enable_green(self,should_enable):
                if should_enable:
                        
                        GPIO.output(GREEN_PIN, True)
                        self.red = False
                        self.green = True
                        #print "enable Green "
                else:
                        GPIO.output(GREEN_PIN, False)
                        self.red = True
                        self.green = False
                        #print "disable red "

led=piClas()
#led.enable_red(True)
while True:
        
        if led.red:
                led.enable_green(True)   
                led.enable_red(False)
                sleep(0.03)
        if led.green:
                led.enable_green(False)   
                led.enable_red(True)
                sleep(0.03)
        

GPIO.cleanup()
